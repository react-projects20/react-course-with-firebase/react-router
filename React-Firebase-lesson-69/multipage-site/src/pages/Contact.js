import {useLocation} from 'react-router-dom'

export default function Contact() {

    const queryString = useLocation().search
    console.log("queryString" + queryString)

    const queryParams = new URLSearchParams(queryString)
    const name = queryParams.get("name")
    console.log("the name"+ name)

    return (
    <div>
        <h2>Hey {name}, Contact Us Here...</h2>
        <p>Contact Lorem ipsum, dolor sit amet consectetur adipisicing elit. Natus, cum repellendus quam porro fuga perspiciatis accusantium suscipit maxime, sed nihil temporibus, nobis cupiditate expedita corporis saepe? Eius earum quae sapiente?</p>
    </div>
    );
}