import { BrowserRouter, Route, Switch, NavLink, Redirect } from 'react-router-dom';
import './App.css'
import About from './pages/About';
import Contact from './pages/Contact';
import Home from './pages/Home';
import Articles from './pages/Articles';


function App() {
  return (
    <div className="App">
      <BrowserRouter>

        <nav>
          <h1>My Articles</h1>
          <NavLink exact to="/">Home</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contact">Contact</NavLink>
        </nav>
        
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>

          <Route exact path="/about">
            <About />
          </Route>

          <Route exact path="/contact">
            <Contact />
          </Route>

          <Route exact path="/articles/:id">
            <Articles/>
          </Route>

          <Route path="*">
            <Redirect to="/"/>
          </Route>
        </Switch>

      </BrowserRouter>
    </div>
  );
}

export default App
